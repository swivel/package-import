// import babel from '@rollup/plugin-babel';
// import terser from '@rollup/plugin-terser';
import resolve from "@rollup/plugin-node-resolve";

const config = {
	input: 'src/index.js',
	output: [
		{ format: 'esm', file: 'lib/index.mjs', sourcemap: true },
		{ format: 'cjs', file: 'lib/index.cjs', sourcemap: true },
	],
	plugins: [
		resolve(),
		// babel({ babelHelpers: 'bundled' }),
		// terser(),
	],
};

export default config;
