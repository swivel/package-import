/* eslint-disable import/prefer-default-export */
import sinon from "sinon";
import crypto from "crypto";

const oldWeakMap = global.WeakMap;
const WeakMapInstance = new WeakMap();

class WeakMapSpy {
	constructor() {
		this.instance = WeakMapInstance;
	}

	/* eslint-disable class-methods-use-this */
	antidote() {
		global.WeakMap = oldWeakMap;
		console.warn("global WeakMap cured");
	}
	/* eslint-enable class-methods-use-this */

	delete(...args) {
		return this.instance.delete(...args);
	}

	has(...args) {
		return this.instance.has(...args);
	}

	set(...args) {
		return this.instance.set(...args);
	}

	get(...args) {
		return this.instance.get(...args);
	}
}

console.warn("Poisoning global WeakMap");
Object.defineProperty(global, "WeakMap", { value: WeakMapSpy, configurable: false, writable: false });

const randomString = (len = 20) => crypto.randomBytes(len).toString("hex");

class FakeFetchResponse {
	constructor() { this.reset(); }

	json() { return Promise.resolve(this.data); }

	reset() {
		this.ok = true;
		this.data = { [randomString()]: randomString() };
	}
}

const fetchSpy = sinon.fake(
	() => Promise.resolve(fetchSpy.mockResponse)
);

fetchSpy.mockResponse = new FakeFetchResponse();

console.warn("Poisoning global fetch()");
Object.defineProperty(global, "fetch", { value: fetchSpy, configurable: false, writable: false });

Object.defineProperty(global, 'window', {
	value: {
		location: new URL(`file://${randomString()}`)
	},
	writable: false,
	configurable: true,
});

export {
	WeakMapSpy,
	randomString,
	FakeFetchResponse,
};
