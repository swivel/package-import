# Package Import

Import ESM packages from registries in the browser.

- [Quick Usage](#quick-usage)
- [Options](#options)
  - [_string_ `modulePath`: Module Resolution Path](#string-modulepath-module-resolution-path)
  - [_object_ `importMap`: Importmaps](#object-importmap-importmaps)
  - [_object_ `registryMap`: Registrymaps](#object-registrymap-registrymaps)
  - [_bool_ `dom = true`: DOM Parsing](#bool-dom--true-dom-parsing)
    - [`<script type="importmap">`](#script-typeimportmap)
    - [`<script type="registrymap">`](#script-typeregistrymap)
- [Script Resolution](#script-resolution)
- [Specifying Versions](#specifying-versions)

## Quick Usage

Package Import provides a simple interface for importing packages using
importmaps and registrymaps to pull packages from their various sources.

```javascript
import PackageImport from "package-import";

const importer = new PackageImport();

await importer.get('my-package');
```

By default, if no `importmap` is defined and no `modulePath` is specified,
Package Import will look at the root of the current website for a
`node_modules` directory.

## Options

```javascript
new PackageImport(modulePath, {
  importMap, // optional
  registryMap, // optional
  dom: true, // optional
});
```

### _string_ `modulePath`: Module Resolution Path

> The default module resolution path is set to `node_modules`, relative to
the base URL of the current site.

To specify the module resolution path, pass a URL to `PackageImport`:

```javascript
const importer = new PackageImport("https://unpkg.com/");
await importer.get("my-package");
```

This will locate the package at `https://unpkg.com/my-package/` and import
the package's `main` script.

### _object_ `importMap`: Importmaps

An importmap can be specified in the constructor, alleviating any
need to add additional script tags on the page:

```javascript
const importer = new PackageImport("https://unpkg.com/", {
  importMap: {
    "imports": {
      "my-package": "https://some-other-registry.com/my-package/index.js"
    }
  }
});

await importer.get("my-package");
```

In the example above, `my-package` always resolving to
`https://some-other-registry.com/my-package/index.js`.

### _object_ `registryMap`: Registrymaps

Registrymaps are similar to
[importmaps](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/script/type/importmap),
except that Package Import will attempt to resolve the package through
its [Script Resolution](#script-resolution) model by first fetching
the module's `package.json` file to discover the main script.

```javascript
const importer = new PackageImport("/node_modules", {
  registryMap: {
    "registries": {
      "unpkg": "https://unpkg.com/"
    },
    "imports": {
      "my-package": "unpkg",
    }
  }
});

await importer.get("my-package");
```

In the example above, `my-package` will be resolved by fetching the
`package.json` from the package that lives at
`https://unpkg.com/my-package/`.

### _bool_ `dom = true`: DOM Parsing

In the event that DOM parsing is not wanted, the `dom` option can be overridden
to `false`. This will prevent the `importmap` and `registrymap` from scanning
the DOM for scripts to use when resolving imports.

```html
<script type="importmap">
{
  "imports": {
    "my-package": "https://unpkg.com/my-package/index.js"
  }
}
</script>
```

```javascript
const importer = new PackageImport("/node_modules", { dom: false });
await importer.get("my-package");
```

In the example above, the module will be resolved from
`/node_modules/my-package` instead of pulling it from `unpkg`.

#### script type="importmap"

Package Import will attempt to parse all scripts with `type="importmap"` on
the page and use it as an
[importmap](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/script/type/importmap)
unless [`dom`](#bool-dom--true-dom-parsing) is set to `false`.

For example, the following:

```html
<script type="importmap">
{
  "imports": {
    "my-package": "https://unpkg.com/my-package/index.js"
  }
}
</script>
```

Will result in `my-package` always resolving to
`https://unpkg.com/my-package/index.js`.

#### script type="registrymap"

While non-standard, Package Import will attempt to parse all scripts with
`type="registrymap"` on the page and use it as a `registrymap`.

For example, the following:

```html
<script type="registrymap">
{
  "registries": {
    "unpkg": "https://unpkg.com/"
  },
  "imports": {
    "my-package": "unpkg",
  }
}
</script>
```

The example above will retrieve `my-package` by fetching the `package.json`
from the package that lives at `https://unpkg.com/my-package/`.

## Script Resolution

Package Import attempts to determine the correct script to import through
a series of steps:

When calling `await importer.get("my-package")`, Package Import will walk through
the following steps:

1. Search any importmap for `"my-package"`.
If it exists, treat it like a direct URL.
2. Search any registrymap for `"my-package"`.
If it exists, use it to resolve the `package.json`.
3. Use the `modulePath` to resolve the `package.json`.

If the `package.json` needs to be resolved, the script is determined by using
checking the `main` property in the `package.json`, or falling back to the
`browser` property in the `package.json`.

## Specifying Versions

Modules with an accompanying [registrymap](#object-registrymap-registrymaps)
will attempt to resolve at the specified version.

```javascript
const importer = new PackageImport("/node_modules", {
  registryMap: {
    "registries": {
      "unpkg": "https://unpkg.com/"
    },
    "imports": {
      "my-package": "unpkg",
    }
  }
});

await importer.get("my-package@12.0.5");
await importer.get("unmapped-pkg@7.5.1");
```

In the example above, `my-package` will be resolved by fetching the
`package.json` from the package that lives at
`https://unpkg.com/my-package@12.0.5/`. However, `unmapped-pkg` will resolve
using the default module resolution path at: `/node_modules/unmapped-pkg`
without the version included in the fetch URL.
