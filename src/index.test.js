/* eslint-disable max-classes-per-file, class-methods-use-this, no-shadow, no-unused-vars */
import assert from "assert";
import sinon from "sinon";
import esmock from "esmock";

import { randomString } from "../test-utils.js";

import PackageImport, { ERRORS } from "./index.js";

describe("package-import::PackageImport", () => {
	afterEach(() => {
		sinon.reset();
	});

	describe("PackageImport::constructor()", () => {
		it("should throw if modulePath isn't a valid string", () => {
			const expected = ERRORS.badModulePath("");

			assert.throws(
				() => new PackageImport(""),
				({ message }) => message.includes(expected)
			);
		});

		it("should pass maps to map constructors", async () => {
			const fakeImportMap = sinon.fake();
			const fakeRegistryMap = sinon.fake();

			const mockImportMapClass = class {
				constructor(...args) {
					fakeImportMap(...args);
				}
			};

			const mockRegistryMapClass = class {
				constructor(...args) {
					fakeRegistryMap(...args);
				}
			};

			const PackageImport = await esmock("./index.js", {
				underwriter: { default: sinon.fake() },
				"./importmap.js": { default: mockImportMapClass },
				"./registrymap.js": { default: mockRegistryMapClass },
			});

			const mockModulePath = `file:///${randomString()}`;
			const mockImportMap = randomString();
			const mockRegistryMap = randomString();

			const _ = new PackageImport(mockModulePath, {
				importMap: mockImportMap,
				registryMap: mockRegistryMap,
				dom: false,
			});

			const importMapCall = fakeImportMap.getCall(0);
			assert.ok(
				importMapCall.calledWith(mockImportMap, false),
				"importmap constructor"
			);

			const registryMapCall = fakeRegistryMap.getCall(0);
			assert.ok(
				registryMapCall.calledWith(mockRegistryMap, false),
				"registrymap constructor"
			);
		});
	});

	describe("PackageImport::get()", () => {
		it("should use importmap if available", async () => {
			const mockPackageUrl = randomString();
			const fakeImportMapFind = sinon.fake(() => mockPackageUrl);

			const mockImportMapClass = class {
				find(...args) {
					return fakeImportMapFind(...args);
				}
			};

			const PackageImport = await esmock("./index.js", {
				"./importmap.js": { default: mockImportMapClass },
				"./registrymap.js": { default: class {} },
			});

			const mockImportMap = randomString();

			const mockInitializer = sinon.fake((...args) => args);
			PackageImport.initializer = mockInitializer;

			const instance = new PackageImport(false, {
				importMap: mockImportMap,
			});

			const mockIdentifier = randomString();
			const actual = await instance.get(mockIdentifier);

			const importMapCall = fakeImportMapFind.getCall(0);
			assert.ok(importMapCall.calledWith(mockIdentifier));

			assert.deepEqual(actual, [mockIdentifier, mockPackageUrl]);
		});

		it("should fallback to registrymap", async () => {
			const mockPackageUrl = `file:///${randomString()}`;
			const fakeRegistryMapFind = sinon.fake(() => mockPackageUrl);

			const mockRegistryMapClass = class {
				find(...args) {
					return fakeRegistryMapFind(...args);
				}
			};

			const fakeImportMapClass = class { find() { return false; } };
			const fakePackageClass = class {
				constructor(_, u) { this.u = u; }

				getScriptUrl() { return this.u; }
			};
			const PackageImport = await esmock("./index.js", {
				"./importmap.js": { default: fakeImportMapClass },
				"./registrymap.js": { default: mockRegistryMapClass },
				"./package.js": { default: fakePackageClass },
			});

			const mockRegistryMap = randomString();

			const mockInitializer = sinon.fake((...args) => args);
			PackageImport.initializer = mockInitializer;

			const instance = new PackageImport(false, {
				registryMap: mockRegistryMap,
			});

			const mockIdentifier = randomString();
			const actual = await instance.get(mockIdentifier);

			const registryMapCall = fakeRegistryMapFind.getCall(0);
			assert.ok(registryMapCall.calledWith(mockIdentifier));

			assert.deepEqual(actual, [
				mockIdentifier, `${mockPackageUrl}/`
			]);
		});

		it("should finally fallback to modulePath", async () => {
			const mockModulePath = `file:///${randomString()}`;

			const fakeImportMapClass = class { find() { return false; } };
			const fakeRegistryMapClass = class { find() { return false; } };
			const fakePackageClass = class {
				constructor(_, u) { this.u = u; }

				getScriptUrl() { return this.u; }
			};

			const PackageImport = await esmock("./index.js", {
				"./importmap.js": { default: fakeImportMapClass },
				"./registrymap.js": { default: fakeRegistryMapClass },
				"./package.js": { default: fakePackageClass },
			});

			const mockInitializer = sinon.fake((...args) => args);
			PackageImport.initializer = mockInitializer;

			const instance = new PackageImport(mockModulePath);

			const mockIdentifier = randomString();
			const actual = await instance.get(mockIdentifier);
			const expectedUrl = `${mockModulePath}/${mockIdentifier}/`;

			assert.deepEqual(actual, [
				mockIdentifier, expectedUrl
			]);
		});
	});
});
