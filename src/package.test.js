/* eslint-disable import/first, no-unused-vars */
import assert from "assert";
import sinon from "sinon";

import { randomString } from "../test-utils.js";

import Package, { ERRORS } from "./package.js";

const mockIdentifier = randomString();

describe("package-import::Package", () => {
	afterEach(() => {
		sinon.reset();
		fetch.mockResponse.reset();
	});

	describe("package::constructor()", () => {
		it("should throw if url is empty", () => {
			const expected = ERRORS.badUrl(mockIdentifier);

			assert.throws(
				() => new Package(mockIdentifier),
				({ message }) => message.includes(expected)
			);
		});
	});

	describe("package::retrieve()", () => {
		it("should fetch the package.json relative to the URL", async () => {
			const mockUrl = `file:///${randomString()}`;
			const instance = new Package(
				mockIdentifier, mockUrl
			);

			fetch.mockResponse.ok = true;

			const actual = await instance.retrieve();

			assert.deepEqual(actual, fetch.mockResponse.data);
			assert.equal(fetch.firstArg, `${mockUrl}/package.json`);
		});

		it("should reject if fetch fails", async () => {
			const mockUrl = `file:///${randomString()}`;
			const instance = new Package(
				mockIdentifier, mockUrl
			);

			const expectedUrl = `${mockUrl}/package.json`;
			const expected = ERRORS.fetchFailed(expectedUrl, mockIdentifier);

			fetch.mockResponse.ok = false;

			await assert.rejects(
				instance.retrieve(),
				({ message }) => message.includes(expected)
			);
		});
	});

	describe("package::getScriptUrl()", () => {
		it("should return a url to the main script", async () => {
			const mockUrl = `file:///${randomString()}`;
			const instance = new Package(
				mockIdentifier, mockUrl
			);

			const mockMainScript = randomString();
			fetch.mockResponse.ok = true;
			fetch.mockResponse.data = { main: mockMainScript };
			const expectedUrl = `${mockUrl}/${mockMainScript}`;

			const actual = await instance.getScriptUrl();

			assert.equal(`${actual}`, expectedUrl);
		});

		it("should reject if package has no scripts", async () => {
			const mockUrl = `file:///${randomString()}`;
			const instance = new Package(
				mockIdentifier, mockUrl
			);

			fetch.mockResponse.ok = true;
			fetch.mockResponse.data = {};

			const expected = ERRORS.noScript(mockIdentifier);

			await assert.rejects(
				instance.getScriptUrl(),
				({ message }) => message.includes(expected)
			);
		});

		it("should reject if package has a bad script", async () => {
			const mockUrl = `file:///${randomString()}`;
			const instance = new Package(
				mockIdentifier, mockUrl
			);

			fetch.mockResponse.ok = true;
			fetch.mockResponse.data = { main: "/" };

			const expected = ERRORS.badScript("/", mockIdentifier);

			await assert.rejects(
				instance.getScriptUrl(),
				({ message }) => message.includes(expected)
			);
		});
	});
});
