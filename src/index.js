import Guarantor from "underwriter";

import Package from "./package.js";
import ImportMap from "./importmap.js";
import RegistryMap from "./registrymap.js";
import { parsePackageId, error } from "./utils.js";

export const ERRORS = {
	badModulePath: (modulePath) => `Module path must not be zero-length: ${modulePath}`,
};

const Private = new WeakMap();

const defaultModulePath = new URL("/node_modules", window.location);

export default class PackageImport {
	/* c8 ignore start */
	static initializer(_, scriptPath) {
		return import(scriptPath);
	}
	/* c8 ignore stop */

	constructor(modulePath, options = {}) {
		const { dom = true } = options;

		if (typeof modulePath === "string" && modulePath.length === 0) {
			error(ERRORS.badModulePath(modulePath));
		}

		const importMap = new ImportMap(options.importMap, dom);

		const registryMap = new RegistryMap(options.registryMap, dom);

		const guarantor = new Guarantor({
			retriever: this.#fetch.bind(this),
			initializer: PackageImport.initializer,
		});

		Private.set(this, {
			modulePath: modulePath || defaultModulePath,
			importMap,
			registryMap,
			guarantor,
		});
	}

	#getModulePath(packageName) {
		const { modulePath } = Private.get(this);
		const { name } = parsePackageId(packageName);
		const cleanModulePath = `${modulePath}/`.replace(/[/]+$/, "/");
		return new URL(`./${name}`, cleanModulePath);
	}

	async #resolve(packageName) {
		const { registryMap } = Private.get(this);

		// Try registrymap
		let packageRoot = registryMap.find(packageName);
		if (!packageRoot) {
			// Otherwise, use module path
			packageRoot = this.#getModulePath(packageName);
		}

		const cleanRoot = `${packageRoot}/`.replace(/[/]+$/, "/");
		const pkg = new Package(packageName, cleanRoot);

		return pkg.getScriptUrl();
	}

	async #fetch(packageName) {
		const { importMap } = Private.get(this);

		// Try importmap first
		const importUrl = importMap.find(packageName);
		if (importUrl) return importUrl;

		// Otherwise, resolve the script path
		return this.#resolve(packageName);
	}

	get(packageName) {
		const { guarantor } = Private.get(this);
		return guarantor.get(packageName);
	}
}
