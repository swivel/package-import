import { parsePackageId, error } from "./utils.js";

export const IMPORTS = "imports";
export const REGISTRIES = "registries";

export const defaultRegistryMap = { [REGISTRIES]: {}, [IMPORTS]: {} };

export const getMapsFromDOM = () => {
	const scripts = document.getElementsByTagName("script");

	const mapSources = Array.from(scripts).filter(
		(s) => s.type === "registrymap"
	);

	const parsedMaps = mapSources.map((script) => {
		try {
			return JSON.parse(script.innerText);
		} catch (_) {
			return false;
		}
	});

	const filteredMaps = parsedMaps.filter(
		(map) => map && IMPORTS in map
	);

	return filteredMaps;
};

export const ERRORS = {
	noRegistriesKey: "Invalid registrymap. Object must have an `registries` property",
	noImportsKey: "Invalid registrymap. Object must have an `imports` property",
	unknownReg: (registryName, packageName) => `Unknown registry "${registryName}" for import "${packageName}"`
};

const Private = new WeakMap();

export default class RegistryMap {
	constructor(customMap, parseDOM = true) {
		if (customMap && !(REGISTRIES in customMap)) {
			error(ERRORS.noRegistriesKey);
		}

		if (customMap && !(IMPORTS in customMap)) {
			error(ERRORS.noImportsKey);
		}

		Private.set(this, {
			customMap: customMap || defaultRegistryMap,
			parseDOM: parseDOM === true,
		});

		this.refresh();
	}

	refresh() {
		const { parseDOM, customMap } = Private.get(this);

		const maps = parseDOM ? getMapsFromDOM() : [defaultRegistryMap];

		// merge the maps
		const registryMap = maps.reduce((m, map) => ({
			[REGISTRIES]: {
				...m[REGISTRIES],
				...map[REGISTRIES],
			},
			[IMPORTS]: {
				...m[IMPORTS],
				...map[IMPORTS],
			}
		}), customMap);

		Private.set(this, {
			...Private.get(this),
			registryMap,
		});

		return Object.freeze(registryMap);
	}

	hasMap(packageName) {
		const { registryMap } = Private.get(this);
		const { [IMPORTS]: imports } = registryMap;
		const { name } = parsePackageId(packageName);
		return name in imports;
	}

	find(packageName) {
		if (!this.hasMap(packageName)) return false;

		const { registryMap } = Private.get(this);

		const {
			[REGISTRIES]: registries,
			[IMPORTS]: imports
		} = registryMap;

		const { qualified, name } = parsePackageId(packageName);

		const registryName = imports[name];
		if (!(registryName in registries)) {
			error(ERRORS.unknownReg(registryName, name), false);
			return false;
		}

		const registryPath = registries[registryName];
		const cleanRegistryPath = `${registryPath}/`.replace(/[/]+$/, "/");
		return new URL(`./${qualified}`, cleanRegistryPath);
	}
}
