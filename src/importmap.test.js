/* eslint-disable import/first, no-unused-vars */
import assert from "assert";
import sinon from "sinon";

import { randomString } from "../test-utils.js";

import ImportMap, {
	defaultImportMap,
	getMapsFromDOM,
	ERRORS
} from "./importmap.js";

describe("package-import::ImportMap", () => {
	afterEach(() => {
		sinon.reset();
	});

	describe("ImportMap::constructor()", () => {
		it("should throw if importmap doesn't have `imports` key", () => {
			const expected = ERRORS.invalidMap;

			assert.throws(
				() => new ImportMap({}, false),
				({ message }) => message.includes(expected)
			);
		});
	});

	describe("ImportMap::refresh()", () => {
		const mockEmptyGetElementsByTagName = sinon.fake(() => []);

		beforeEach(() => {
			Object.defineProperty(global, "document", {
				value: {
					getElementsByTagName: mockEmptyGetElementsByTagName
				},
				writable: false,
				configurable: true,
			});
		});

		it("should use getMapsFromDOM() if parseDOM is true", () => {
			const _ = new ImportMap(false, true);

			assert.equal(mockEmptyGetElementsByTagName.firstArg, "script");
		});

		it("should default to defaultImportMap", () => {
			const instance = new ImportMap(false, true);
			const map = instance.refresh();
			assert.deepEqual(map, defaultImportMap);
		});

		it("should merge multiple maps", () => {
			const firstMap = { [randomString()]: randomString() };
			const secondMap = { [randomString()]: randomString() };

			const mockGetElementsByTagName = sinon.fake(() => [
				{
					type: "importmap",
					innerText: JSON.stringify({
						imports: firstMap
					})
				}
			]);

			Object.defineProperty(global, "document", {
				value: {
					getElementsByTagName: mockGetElementsByTagName
				},
				writable: false,
				configurable: true,
			});

			const expected = {
				imports: {
					...firstMap,
					...secondMap,
				}
			};

			const instance = new ImportMap({
				imports: secondMap
			}, true);

			const map = instance.refresh();

			assert.deepEqual(map, expected);
		});
	});

	describe("ImportMap::find()", () => {
		const mockIdentifier = randomString();
		const mockUrl = `file:///${randomString()}`;

		const instance = new ImportMap({
			imports: {
				[mockIdentifier]: mockUrl
			}
		}, false);

		it("should return entry in importmap", () => {
			const expected = mockUrl;
			const actual = instance.find(mockIdentifier);
			assert.equal(`${actual}`, expected);
		});

		it("should return false is no package is found", () => {
			const actual = instance.find(randomString());
			assert.equal(actual, false);
		});
	});

	describe("getMapsFromDOM", () => {
		before(() => {
			const mockDocument = {
				getElementsByTagName: sinon.fake(
					() => mockDocument.mockScripts
				)
			};

			mockDocument.mockScripts = [];

			Object.defineProperty(global, "document", {
				value: mockDocument,
				writable: false,
				configurable: true,
			});
		});

		it("should cleanly return an arrow of importmap objects", () => {
			const expectedFirst = { [randomString()]: randomString() };
			const expectedSecond = { [randomString()]: randomString() };

			document.mockScripts = [
				{ type: "importmap", innerText: "{ \"x\": \"y\" }" },
				{ type: "importmap", innerText: null },
				{ type: "importmap", innerText: "jifoeijoeaf" },
				{
					type: "importmap",
					innerText: JSON.stringify({
						imports: { ...expectedFirst }
					})
				},
				{
					type: "importmap",
					innerText: JSON.stringify({
						imports: { ...expectedSecond }
					})
				}
			];

			const expected = [
				{ imports: expectedFirst },
				{ imports: expectedSecond },
			];

			const actual = getMapsFromDOM();

			assert.deepEqual(actual, expected);
		});
	});
});
