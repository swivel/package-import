/* eslint-disable import/first, no-unused-vars */
import assert from "assert";
import sinon from "sinon";

import { randomString } from "../test-utils.js";

import RegistryMap, {
	defaultRegistryMap,
	getMapsFromDOM,
	ERRORS
} from "./registrymap.js";

describe("package-import::RegistryMap", () => {
	afterEach(() => {
		sinon.reset();
	});

	describe("RegistryMap::constructor()", () => {
		it("should throw if registrymap doesn't have `registries` key", () => {
			const expected = ERRORS.noRegistriesKey;

			assert.throws(
				() => new RegistryMap({}, false),
				({ message }) => message.includes(expected)
			);
		});

		it("should throw if registrymap doesn't have `imports` key", () => {
			const expected = ERRORS.noImportsKey;

			assert.throws(
				() => new RegistryMap({ registries: {} }, false),
				({ message }) => message.includes(expected)
			);
		});
	});

	describe("RegistryMap::refresh()", () => {
		const mockEmptyGetElementsByTagName = sinon.fake(() => []);

		beforeEach(() => {
			Object.defineProperty(global, "document", {
				value: {
					getElementsByTagName: mockEmptyGetElementsByTagName
				},
				writable: false,
				configurable: true,
			});
		});

		it("should use getMapsFromDOM() if parseDOM is true", () => {
			const _ = new RegistryMap(false, true);

			assert.equal(mockEmptyGetElementsByTagName.firstArg, "script");
		});

		it("should default to defaultRegistryMap", () => {
			const instance = new RegistryMap(false, true);
			const map = instance.refresh();
			assert.deepEqual(map, defaultRegistryMap);
		});

		it("should merge multiple maps", () => {
			const firstMap = { [randomString()]: randomString() };
			const secondMap = { [randomString()]: randomString() };

			const mockGetElementsByTagName = sinon.fake(() => [
				{
					type: "registrymap",
					innerText: JSON.stringify({
						imports: firstMap,
						registries: firstMap,
					})
				}
			]);

			Object.defineProperty(global, "document", {
				value: {
					getElementsByTagName: mockGetElementsByTagName
				},
				writable: false,
				configurable: true,
			});

			const expected = {
				imports: { ...firstMap, ...secondMap },
				registries: { ...firstMap, ...secondMap },
			};

			const instance = new RegistryMap({
				imports: secondMap,
				registries: secondMap,
			}, true);

			const map = instance.refresh();

			assert.deepEqual(map, expected);
		});
	});

	describe("RegistryMap::hasMap()", () => {
		const mockRegistry = randomString();
		const mockIdentifier = randomString();
		const mockUrl = `file:///${randomString()}`;

		const instance = new RegistryMap({
			imports: {
				[mockIdentifier]: mockRegistry,
			},
			registries: {
				[mockRegistry]: mockUrl,
			},
		}, false);

		it("should return false is package isn't in registrymap", () => {
			const actual = instance.hasMap(randomString());
			assert.equal(actual, false);
		});

		it("should return true is package isn't in registrymap", () => {
			const actual = instance.hasMap(mockIdentifier);
			assert.equal(actual, true);
		});
	});

	describe("RegistryMap::find()", () => {
		const mockRegistry = randomString();
		const mockIdentifier = randomString();
		const mockIdentifierBadReg = randomString();
		const mockUrl = `file:///${randomString()}`;

		const instance = new RegistryMap({
			imports: {
				[mockIdentifier]: mockRegistry,
				[mockIdentifierBadReg]: randomString(),
			},
			registries: {
				[mockRegistry]: mockUrl,
			},
		}, false);

		it("should return entry in registrymap", () => {
			const expected = `${mockUrl}/${mockIdentifier}@latest`;
			const actual = instance.find(mockIdentifier);
			assert.equal(`${actual}`, expected);
		});

		it("should return false is package isn't in registrymap", () => {
			const actual = instance.find(randomString());
			assert.equal(actual, false);
		});

		it("should return false is package's registry isn't in registrymap", () => {
			const actual = instance.find(mockIdentifierBadReg);
			assert.equal(actual, false);
		});
	});

	describe("getMapsFromDOM", () => {
		before(() => {
			const mockDocument = {
				getElementsByTagName: sinon.fake(
					() => mockDocument.mockScripts
				)
			};

			mockDocument.mockScripts = [];

			Object.defineProperty(global, "document", {
				value: mockDocument,
				writable: false,
				configurable: true,
			});
		});

		it("should cleanly return an arrow of registrymap objects", () => {
			const expectedFirst = { [randomString()]: randomString() };
			const expectedSecond = { [randomString()]: randomString() };

			document.mockScripts = [
				{ type: "registrymap", innerText: "{ \"x\": \"y\" }" },
				{ type: "registrymap", innerText: null },
				{ type: "registrymap", innerText: "jifoeijoeaf" },
				{
					type: "registrymap",
					innerText: JSON.stringify({
						imports: { ...expectedFirst },
						registries: { ...expectedFirst },
					})
				},
				{
					type: "registrymap",
					innerText: JSON.stringify({
						imports: { ...expectedSecond },
						registries: { ...expectedSecond },
					})
				}
			];

			const expected = [
				{ imports: expectedFirst, registries: expectedFirst },
				{ imports: expectedSecond, registries: expectedSecond },
			];

			const actual = getMapsFromDOM();

			assert.deepEqual(actual, expected);
		});
	});
});
