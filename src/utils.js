const REG_PKG_NAME = /^((?:@[^\s/@]+\/)?[^\s/@]+)(?:@([^\s/@]+))?$/;

export const parsePackageId = (packageName) => {
	const [, name, version = "latest"] = `${packageName}`.match(REG_PKG_NAME);
	return {
		name,
		version,
		qualified: `${name}@${version}`,
	};
};

export const error = (message, fatal = true) => {
	const prepended = `PackageImport: ${message}`;
	console.error(prepended);
	if (fatal) throw new Error(prepended);
};
