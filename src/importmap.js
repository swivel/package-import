import { error, parsePackageId } from "./utils.js";

export const IMPORTS = "imports";

export const defaultImportMap = { [IMPORTS]: {} };

export const getMapsFromDOM = () => {
	const scripts = document.getElementsByTagName("script");

	const mapSources = Array.from(scripts).filter(
		(s) => s.type === "importmap"
	);

	const parsedMaps = mapSources.map((script) => {
		try {
			return JSON.parse(script.innerText);
		} catch (_) {
			return false;
		}
	});

	const filteredMaps = parsedMaps.filter(
		(map) => map && IMPORTS in map
	);

	return filteredMaps;
};

export const ERRORS = {
	invalidMap: "Invalid importmap. Object must have an `imports` property",
};

const Private = new WeakMap();

export default class ImportMap {
	constructor(customMap, parseDOM = true) {
		if (customMap && !(IMPORTS in customMap)) {
			error(ERRORS.invalidMap);
		}

		Private.set(this, {
			customMap: customMap || defaultImportMap,
			parseDOM: parseDOM === true,
		});

		this.refresh();
	}

	refresh() {
		const { parseDOM, customMap } = Private.get(this);

		const maps = parseDOM ? getMapsFromDOM() : [defaultImportMap];

		// merge the maps
		const importMap = maps.reduce((m, map) => ({
			[IMPORTS]: {
				...m[IMPORTS],
				...map[IMPORTS],
			}
		}), customMap);

		Private.set(this, {
			...Private.get(this),
			importMap,
		});

		return Object.freeze(importMap);
	}

	find(packageName) {
		const { importMap: { imports } } = Private.get(this);

		const { name } = parsePackageId(packageName);

		if (!(name in imports)) return false;

		return new URL(imports[name]);
	}
}
