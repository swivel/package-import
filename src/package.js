import { error } from "./utils.js";

const PACKAGE_JSON = "package.json";

export const cleanLeadingSlashes = (path) => path.replace(/^[/]*(.*)$/g, "$1");

const Private = new WeakMap();

export const ERRORS = {
	badUrl: (packageName) => `Bad URL for package: ${packageName}`,
	fetchFailed: (packageUri, packageName) => `Error retrieving package for ${packageName}: ${packageUri}`,
	noScript: (packageName) => `Malformed package.json for ${packageName}: No main or browser script defined.`,
	badScript: (scriptName, packageName) => `Bad script path in package.json for ${packageName}: "${scriptName}"`,
};

export default class Package {
	constructor(packageName, url) {
		/* c8 ignore start */
		if (typeof packageName !== "string" || packageName.length === 0) {
			console.warn("PackageName is recommended for Package.");
		}
		/* c8 ignore end */

		if (typeof url !== "string" || url.length === 0) {
			error(ERRORS.badUrl(packageName));
		}

		Private.set(this, { url, packageName });
	}

	async retrieve() {
		const { url, packageName } = Private.get(this);
		const cleanRoot = `${url}/`.replace(/[/]+$/, "/");
		const packageUri = new URL(`./${PACKAGE_JSON}`, cleanRoot);

		const response = await fetch(packageUri);

		if (!response.ok) {
			error(ERRORS.fetchFailed(packageUri, packageName));
		}

		return response.json();
	}

	async getScriptUrl() {
		const { packageName } = Private.get(this);

		const { main, browser } = await this.retrieve();

		const scriptName = main || browser;
		if (typeof scriptName !== "string" || scriptName.length === 0) {
			error(ERRORS.noScript(packageName));
		}

		const scriptPath = cleanLeadingSlashes(scriptName);
		if (typeof scriptPath !== "string" || scriptPath.length === 0) {
			error(ERRORS.badScript(scriptName, packageName));
		}

		const { url } = Private.get(this);
		const cleanRoot = `${url}/`.replace(/[/]+$/, "/");
		return new URL(`./${scriptPath}`, cleanRoot);
	}
}
